import axios from 'axios'
import {
  FETCH_LIST_REQUEST,
  FETCH_LIST_SUCCESS,
  FETCH_LIST_FAILURE,
  FETCH_TABLE_REQUEST,
  FETCH_TABLE_SUCCESS,
  FETCH_TABLE_FAILURE,
  FETCH_SHOW_SUCCESS,
  FETCH_ICON,
  FETCH_FILTERDROPDOWN,

} from './AppListTypes'
export const fetchLists = () => {
  return (dispatch) => {
    dispatch(fetchListsRequest())
    axios
      .post('http://education.faranegar.com/api/LogSelectDBs')
      .then(response => {

        const Lists = response.data.Body.ResultList;
        dispatch(fetchListsSuccess(Lists))
      })
      .catch(error => {
        // error.message is the error message
        dispatch(fetchListsFailure(error.message))
      })
  }
}

export const fetchsecondList =(DBName)=>{
  return (dispatch) => { 
    dispatch(fetchShow(true))
    dispatch(fetchsecondListRequest())
  axios.post('http://education.faranegar.com/api/SelectLogByDB',{DBName})
  .then(response => {
    console.log(response)
    console.log('dhfcg')
      const secondList = response.data.ResultList;  
      dispatch(fetchsecondListSuccess(secondList))
      dispatch(fetchfilterDropDown(secondList))
   
  })
  .catch(error => {
      dispatch(fetchsecondListFailure(error.message))
  })
}

}

export const  sortAscDate =(secondList) =>{
  return (dispatch) => {
    dispatch(fetchIcon(false))
    let b = secondList.sort((a,b)=>{
      if(a.CreatedAtDateTime > b.CreatedAtDateTime)
        return 1
      else if(a.CreatedAtDateTime < b.CreatedAtDateTime)
        return -1
      else
        return 0
    })
  
    dispatch(fetchsecondListSuccess(b));

}
}
export const  sortDesDate =(secondList) =>{
  return (dispatch) => {
     dispatch(fetchIcon(true))
    let b = secondList.sort((a,b)=>{
      if(a.CreatedAtDateTime > b.CreatedAtDateTime)
        return -1
      else if(a.CreatedAtDateTime < b.CreatedAtDateTime)
        return 1
      else
        return 0
    })
    dispatch(fetchsecondListSuccess(b));

}
}

export const fetchfilterDropDown = (filterDropDown) => {
  return {
    type: FETCH_FILTERDROPDOWN,
    payload: filterDropDown
  
  }
}

export const fetchIcon=(icon)=>{
  return{
    type:FETCH_ICON,
    payload:icon
  }
}

export const fetchShow=(show)=>{
  return{
    type:FETCH_SHOW_SUCCESS,
    payload:show
  }
}
export const fetchListsRequest = () => {
  return {
    type: FETCH_LIST_REQUEST
  }
}

export const fetchListsSuccess = Lists => {
  return {
    type: FETCH_LIST_SUCCESS,
    payload: Lists
  }
}

export const fetchListsFailure = error => {
  return {
    type: FETCH_LIST_FAILURE,
    payload: error
  }
}
export const fetchsecondListRequest = () => {
  return {
    type: FETCH_TABLE_REQUEST
  }
}

export const fetchsecondListSuccess = (secondList) => {
  return {
    type: FETCH_TABLE_SUCCESS,
    payload: secondList
  
  }
}

export const fetchsecondListFailure = error => {
  return {
    type: FETCH_TABLE_FAILURE,
    payload: error
  }
}
