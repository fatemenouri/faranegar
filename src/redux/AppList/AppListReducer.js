import {
  FETCH_LIST_REQUEST,
  FETCH_LIST_SUCCESS,
  FETCH_LIST_FAILURE,
  FETCH_TABLE_REQUEST,
  FETCH_TABLE_SUCCESS,
  FETCH_TABLE_FAILURE,
  FETCH_SHOW_SUCCESS,
  FETCH_ICON,
  FETCH_FILTERDROPDOWN,
 


} from './AppListTypes'

export const initialState = {
  loading: false,
  load:false,
  Lists: [],
  secondList: [],
  error: '',
  show:false,
  logtext:'',
  sortIcon:true,
  icon2:false,
  filterDropdown:[]
  

}

const reducer = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_LIST_REQUEST:
      return {
        ...state,
        loading: true
      }
    case FETCH_LIST_SUCCESS:
      return {
        ...state,
        loading: false,
        Lists: action.payload,
        error: ''
      }
    case FETCH_LIST_FAILURE:
      return {
        ...state,
        loading: false,
        Lists: [],
        error: action.payload
      }
      case FETCH_TABLE_REQUEST:
        return {
          ...state,
          load: true
        }
      case FETCH_TABLE_SUCCESS:
        return {
          ...state,
          load: false,
          secondList: action.payload,
          error: ''
        }
      case FETCH_TABLE_FAILURE:
        return {
          ...state,
          load: false,
          secondList: [],
          error: action.payload
        }
        case FETCH_SHOW_SUCCESS:
            return {
              ...state,
            show: action.payload
            }
            case FETCH_ICON:
              return {
                ...state,
                sortIcon: action.payload
              }
          
              case  FETCH_FILTERDROPDOWN:
                return{
                  ...state,
                 filterDropdown:action.payload
                }
   
     
    default: return state
  }
}

export default reducer
