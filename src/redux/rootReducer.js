import { combineReducers } from 'redux'
import AppListReducer from './AppList/AppListReducer'

const rootReducer = combineReducers({
  app: AppListReducer
})

export default rootReducer
