import React from 'react'
import { connect } from 'react-redux'
import { sortDesDate, sortAscDate, fetchfilterDropDown } from '../redux/AppList/AppListActions'
import './showlist.css'
import { Button } from '@material-ui/core'
import Card from 'react-bootstrap/Card'
import FormControl from '@material-ui/core/FormControl';
import Radio from '@material-ui/core/Radio';
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import CircularProgress from '@material-ui/core/CircularProgress';
import DoneIcon from '@material-ui/icons/Done';
import ArrowDownwardIcon from '@material-ui/icons/ArrowDownward';
import ArrowUpwardIcon from '@material-ui/icons/ArrowUpward';
import TablePagination from '@material-ui/core/TablePagination';
import Container from 'react-bootstrap/Container';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
function ReqListContainer(
  {

    show,
    secondList,
    sortAscDate,
    sortDesDate,
    sortIcon,
    fetchfilterDropDown,
    filterDropdown,
    load,
    error

  }
) {

  const [value, setValue] = React.useState();
  const handleChangeLogType = event => {
    setValue(event.target.value);
    fetchfilterDropDown(secondList.filter(function (result) {
      return result.LogType === event.target.value;
    }))

    if (event.target.value === 'both') {
      fetchfilterDropDown(secondList);
    }
  }

  const moment = require('moment');
  const [open, setOpen] = React.useState(false);
  const [logtext, setLogtext] = React.useState();
  const [done, setDone] = React.useState(false);
  const [icon, setIcon] = React.useState(false);
  const handleClickOpen = (string) => {
    const str = JSON.parse(string)
    if (str.ResultCode) {
      setIcon(true);
      if (str.ResultCode === 1) {
        setDone(true)
        setOpen(true);
        setLogtext(string);
      }
      else {
        setDone(false);
        setOpen(true);
        setLogtext(string);
      }
    }
    else {
      setIcon(false);
      setOpen(true);
      setLogtext(string);
    }
  }
  const handleClose = () => {
    setOpen(false);
  };
  const [page, setPage] = React.useState(0);
  const [rowsPerPage, setRowsPerPage] = React.useState(10);

  const handleChangePage = (event, newPage) => {
    setPage(newPage);
  };

  const handleChangeRowsPerPage = (event) => {
    setRowsPerPage(+event.target.value);
    setPage(0);
  };

  return (

    load ? (
      <div className={'bigcard'} >
        <CircularProgress />
      </div>
    ) : error ? (
      <h2>{error}</h2>
    ) : (

          <div id={show ? 'show' : 'hide'}>
            <Card variant="outlined" style={{ backgroundColor: "#e3f2fd" }} className={'bigcard'} >
              <Card.Body>
                <Card.Title>LogType</Card.Title>

                <>
                  <FormControl component="fieldset">
                  
                    <RadioGroup aria-label="gender" value={value} onChange={handleChangeLogType}>
                      <div>
                      <FormControlLabel value="Response" control={<Radio />} label="Response" />
                      <FormControlLabel value="Request" control={<Radio />} label="Request" />
                      <FormControlLabel value="both" control={<Radio />} label="both" /></div>

                    </RadioGroup>
                  </FormControl>
                </>
                <Card
                  style={{
                    borderColor: 'purple',
                    borderWidth: 3
                  }}
                  className={'content'}
                >
                  <Container>
                    <Row xs={12} sm={12} md={12} lg={12} xl={12} >
                      <Col style={{ textAlign: 'center' ,padding:'10px' , color:'purple'}} xs={12} lg={3}>
                        {sortIcon ?
                         <ArrowDownwardIcon id={'showIcon1'} color='action'
                         onClick={() => sortAscDate(filterDropdown)} fontSize='small' /> :
                         <ArrowUpwardIcon id={'showIcon2'} color='action'
                          onClick={() => sortDesDate(filterDropdown)} fontSize='small' />
                        }
                      CreatedAtDateTime</Col>

                      <Col style={{ textAlign: 'center' ,padding:'10px',color:'purple' }} xs={12} lg={3}>
                        DBName</Col>
                      <Col style={{ textAlign: 'center' ,padding:'10px',color:'purple'}} xs={12} lg={3}>
                        Controller</Col>
                      <Col style={{ textAlign: 'center',padding:'10px',color:'purple' }} xs={12} lg={3}>
                        LogText</Col>
                    </Row>
                  </Container>
                </Card>
                <div style={{ height: '400px', overflowY: 'scroll' }}>
                  {filterDropdown.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((list) => (
                    <Card
                      className={'content '}
                      style={{ borderLeftColor: 'purple', borderLeftWidth: 5 }}
                    >
                      <Container>
                        <Row key={list.CreatedAtDateTime} xs={12} sm={12} md={12} lg={12} xl={12}>
                          <Col style={{  textAlign: 'center' ,padding:'10px'}}
                           xs={12} lg={3}
                          >
                            {moment(list.CreatedAtDateTime).format("dddd, MMMM Do YYYY LTS")}</Col>
                          <Col style={{ textAlign: 'center' ,padding:'10px' }}
                           xs={12} lg={3}
                          >{list.DBName}</Col>
                          <Col style={{  textAlign: 'center' ,padding:'10px' }}
                           xs={12} lg={3}
                          >{list.Controller}</Col>

                          <Col style={{  textAlign: 'center' ,padding:'10px'}}
                           xs={12} lg={3}
                          >
                            <Button variant="outlined"
                              size='medium'
                              color="inherit"
                              onClick={() => handleClickOpen(list.LogText)}>
                              click here
                               </Button>
                          </Col>
                        </Row>
                      </Container>
                    </Card>
                  ))}
                </div>
              </Card.Body>
              <Dialog
                open={open}
                onClose={handleClose}

              >
                <DialogTitle >{"Log Text"}</DialogTitle>
                <DialogContent>
                  <DialogContentText >
                    <div id={icon ? 'en' : 'dis'} >
                      <DoneIcon id={done ? 'done' : 'reject'} /></div>
                    {logtext}
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={handleClose} color="primary">
                    close
                    </Button>
                </DialogActions>
              </Dialog>
              <TablePagination
                rowsPerPageOptions={[3, 5, 10]}
                component="div"
                count={filterDropdown.length}
                rowsPerPage={rowsPerPage}
                page={page}
                onChangePage={handleChangePage}
                onChangeRowsPerPage={handleChangeRowsPerPage}
              />
            </Card>
          </div>
        )
  )
}
const mapStateToProps = state => {

  return {
    load: state.app.load,
    error: state.app.error,
    logType: state.app.logType,
    show: state.app.show,
    secondList: state.app.secondList,
    sortIcon: state.app.sortIcon,
    filterDropdown: state.app.filterDropdown


  }

}

const mapDispatchToProps = dispatch => {
  return {
    sortAscDate: (secondList) => dispatch(sortAscDate(secondList)),
    sortDesDate: (secondList) => dispatch(sortDesDate(secondList)),
    fetchfilterDropDown: (s) => dispatch(fetchfilterDropDown(s)),
  }
}
export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ReqListContainer)
