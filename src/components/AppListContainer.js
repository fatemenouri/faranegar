import React, { useEffect } from 'react'
import { connect } from 'react-redux'
import { fetchsecondList, fetchLists} from '../redux/AppList/AppListActions'
import './showlist.css'
import { withStyles} from '@material-ui/core/styles';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import ReqListContainer from './ReqListContainer';
import CircularProgress from '@material-ui/core/CircularProgress';
import './showlist.css';
import { makeStyles } from '@material-ui/core/styles';
import InputLabel from '@material-ui/core/InputLabel';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';


const StyledTableCell = withStyles((theme) => ({
  head: {
    textAlign: "center",
  }
}))(TableCell);
const StyledTableRow = withStyles((theme) => ({
  root: {
    '&:nth-of-type(odd)': {
      backgroundColor: 'white',
    },

  },
}))(TableRow);
const useStyles = makeStyles((theme) => ({
  button: {
    display: 'block',
    marginTop: theme.spacing(2),
  },
  formControl: {
    margin: theme.spacing(1),
    minWidth: 200,

  },
}));
function AppListsContainer({
  appLists,
  loading,
  er,
  fetchLists,
  fetchsecondList

}){
  const [open, setOpen] = React.useState(false);
  const handleClose = () => {
    setOpen(false);
  };

  const handleOpen = () => {
    setOpen(true);
  };
  useEffect(() => {
    fetchLists()
    document.body.style.backgroundColor = "#f5f5f5"
  }, [fetchLists])
  const classes = withStyles()
  const styles = useStyles();
  
  return ( loading ? (

    <div className={'bigcard'}  style={{ marginTop: '100px'}}>
     <CircularProgress  />
     </div>
    ) : er ? (
      <h2>{er}</h2>
    ) : (
      <div >
        <TableContainer component={Paper} style={{
          textAlign: "center",
          marginTop: '80px',
          width: '250px'
        }}
          className={'bigcard'}
        >

          <Table bordered hover size="sm" style={{ backgroundColor: "#e3f2fd" }} >
            <TableHead >
              <TableRow>
                <StyledTableCell className={classes.head}>App List</StyledTableCell>
              </TableRow></TableHead>
            <TableBody>
              {appLists.map(list =>
                <StyledTableRow key={list.DBName}>
                  <StyledTableCell >
      <FormControl className={styles.formControl}>
        <InputLabel id="demo-controlled-open-select-label">app</InputLabel>
        <Select
          labelId="demo-controlled-open-select-label"
          id="demo-controlled-open-select"
          open={open}
          onClose={handleClose}
          onOpen={handleOpen}
          onChange={()=>fetchsecondList(list.DBName)}
        >
          <MenuItem value={list.DBName} >{list.DBName}</MenuItem>
        </Select>
      </FormControl>
                 
                    </StyledTableCell>
                </StyledTableRow>
              )}
            </TableBody>
          </Table>  </TableContainer>
          <br/>
      <ReqListContainer/>
      </div>
    )
  )
}

const mapStateToProps = state => {

  return {
    appLists: state.app.Lists,
    loading:state.app.loading,
    er:state.app.error
  }

}

const mapDispatchToProps = dispatch => {
  return {
    fetchLists: () => dispatch(fetchLists()),
    fetchsecondList: () => dispatch(fetchsecondList()),
 
    
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(AppListsContainer)
