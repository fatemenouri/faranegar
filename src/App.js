import React  from 'react'
import { Provider } from 'react-redux'
import './App.css';
import store from './redux/store'
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import Home from './components/Home'
import ShowLists from './components/ShowLists'
import  ResponsiveDrawer from './components/SideBar'
function App () {
 
  return (
  <BrowserRouter >
    <Provider store={store}>
     <ResponsiveDrawer/>
   
       <Switch>   
        <Route path='/Home' component={Home} exact />
        <Route path='/'  component={ShowLists} />
       </Switch>
     
    </Provider> </BrowserRouter>
  )
}

export default App
